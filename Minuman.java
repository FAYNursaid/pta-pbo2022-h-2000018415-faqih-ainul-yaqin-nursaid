public class Minuman extends Makanan{
    public int EsTeh,
            EsJeruk,
            orang;

    public Minuman(int Ayam, int Kambing, int GuleKambing, int Teh, int Jeruk) {
        super(Ayam, Kambing, GuleKambing);
        EsTeh = Teh;
        EsJeruk = Jeruk;
    }
    protected void setOrg(int org) {
        orang = org;
    }
    protected int getOrg() {
        return orang;
    }
    protected int hargaTeh() {
        int total = EsTeh * orang;
        return total;
    }
    protected int hargaJeruk() {
        int total = EsJeruk * orang;
        return total;
    }
}